# Muvnday intern exercise

A web application that lets the user create ad delete tasks

# To run locally 
1. Go into the directory of this project and run `npm install`
2. run `npm start`. The Application will run on http://localhost:3000.

# If you don't want to run locally 
The application is deployed using AWS s3.
Use this link: <http://muvnday-challenge.s3-website-us-west-1.amazonaws.com>.

Tools used
---

* React
* Redux
* Firebase
