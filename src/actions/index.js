
import { database } from './../config/fire'

let todosCollection = database.collection("todos");


export const setUser = (payload) => {
    return {
        type: 'SET_USER',
        email: payload
    }
}

export const signOut = ()=>{
    return {
        type: 'SIGN_OUT',
    }
}
export const addTodo = ({ title, description, dueDate, user }) => async (dispatch) => {
    todosCollection.add({
        title,
        description,
        dueDate,
        user,
        completed: false
    }).then((document) => {
        dispatch({
            type: 'ADD_TODO',
            title,
            description,
            dueDate,
            id: document.id,
            user
        }
        )
    });
}

export const fetchTodos = (payload) => async (dispatch) => {
    todosCollection.where('user', '==', payload).get().then(function (querySnapshot) {
        let todos = []
        querySnapshot.forEach((snapshot) => {
            let { description, title, dueDate, completed, user } = snapshot.data();
            let id = snapshot.id
            todos.push({ id, description, title, dueDate, completed, user })
        });
        dispatch({
            type: 'FETCH_TODO',
            todos,
        })
    });


}

export const deleteTodo = (payload) => async (dispatch) => {
    todosCollection.doc(payload.id).delete()
    dispatch({
        type: 'DELETE_TODO',
        index: payload.index
    })
}