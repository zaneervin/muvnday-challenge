import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import TodoItem from './TodoItem'
import { useSelector } from 'react-redux'

function Home() {
    let todos = useSelector(state => state.todos)
    let user = useSelector(state => state.user)

    if (!user) {
        return <Redirect to='/login' />
    }
    return (

        <div className="container mt-5">
            <h1>A list of your tasks</h1>
            <div className="row justify-content-center">
                <div className="col-10 col-sm-12 col-md-8">
                    {todos.length > 0 ? (
                        todos.map((todo, index) => (
                            <TodoItem index={index} id={todo.id} title={todo.title} description={todo.description} dueDate={todo.dueDate} />
                        ))
                    ) : (
                            <p className="noTodosMessage"> You don't have any tasks</p>
                        )}

                </div>
            </div>

            <Link className="btn btn-primary" to='/newTask'>New Task</Link>
        </div>

    )
}

export default Home