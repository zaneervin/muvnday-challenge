import React from 'react'

import { deleteTodo} from './../actions/'
import { useDispatch } from 'react-redux'

function TodoItem(props) {
    let dispatch = useDispatch();
    return (
        <div>
            <div className="todoItem">
                <div className="todoItem__header">
                    < span className="todoItem__title">{props.title}</ span>
                    < span className="todoItem__dueDate">Due date: {props.dueDate}</span>
                </div>
                < div className="todoItem__description">{props.description}
            </div> 
                <div className="todoItem__footer">
                    <button className="btn btn-danger" onClick={()=>dispatch(deleteTodo({id:props.id,index:props.index}))} >Delete</button>
                </div>
            </div>
        </div>

    )
}

export default TodoItem