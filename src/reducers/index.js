import { combineReducers } from 'redux'
const todosReducer = (state = [], action) => {
    switch (action.type) {
        case 'ADD_TODO':
            return [
                ...state, {
                    id: action.id,
                    title: action.title,
                    description: action.description,
                    dueDate: action.dueDate,
                    completed: false
                }
            ]
            case 'DELETE_TODO':
                    let newState = [...state];
                    newState.splice(action.index, 1);
                    return newState;
            case 'FETCH_TODO':
                return action.todos
            default:
                return state
    }
}

const userReducer =(state='',action) =>{
    switch(action.type){
        case 'SET_USER':
            return action.email
        case 'SIGN_OUT':
            return ''
        default:
            return state
    }

}

const rootReducer = combineReducers({
    todos: todosReducer,
    user: userReducer

})

export default rootReducer